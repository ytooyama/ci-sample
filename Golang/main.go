package main

import (
	"fmt"
	"html"
	"log"
	"net/http"
)

//Greet are Say Hello
func Greet() {
	fmt.Println("Hello, World!")
}

func main() {
	Greet()
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
	})
	log.Fatal(http.ListenAndServe(":50080", nil))
}
