# golang CI demo

## Test Step

- go vet
- golint
- gofmt

## Build Step

- docker build the images
- docker push to Local registory